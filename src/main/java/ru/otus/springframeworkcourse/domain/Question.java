package ru.otus.springframeworkcourse.domain;

import java.util.List;

public class Question {

    private final String question;
    private final List<String> variantsOfAnswers;
    private final int correctAnswer;

    public Question(String question, List<String> variantsOfAnswers, int correctAnswer) {
        this.question = question;
        this.variantsOfAnswers = variantsOfAnswers;
        this.correctAnswer = correctAnswer;
    }

    public String getQuestion() {
        return question;
    }

    public List<String> getVariantsOfAnswers() {
        return variantsOfAnswers;
    }

    public boolean isCorrectAnswer(int answer) {
        return answer == correctAnswer;
    }
}
