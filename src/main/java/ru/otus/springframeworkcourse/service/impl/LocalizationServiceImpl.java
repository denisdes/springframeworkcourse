package ru.otus.springframeworkcourse.service.impl;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import ru.otus.springframeworkcourse.config.ApplicationSettings;
import ru.otus.springframeworkcourse.service.LocalizationService;

import java.util.Locale;

@Service
public class LocalizationServiceImpl implements LocalizationService {

    private final MessageSource messageSource;
    private final String lang;
    private final String country;

    public LocalizationServiceImpl(MessageSource messageSource, ApplicationSettings applicationSettings) {
        this.messageSource = messageSource;

        String locale = applicationSettings.getLocale();
        String[] langAndCountry = locale.split("_");
        if (langAndCountry.length == 2) {
            this.lang = langAndCountry[0];
            this.country = langAndCountry[1];
        } else {
            this.lang = "ru";
            this.country = "RU";
        }
    }

    @Override
    public String getLocalizedMessage(String messageKey, Object... args) {
        return messageSource.getMessage(messageKey, args, new Locale(lang, country));
    }
}
