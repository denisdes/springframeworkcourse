package ru.otus.springframeworkcourse.service.impl;

import org.springframework.stereotype.Service;
import ru.otus.springframeworkcourse.config.ApplicationSettings;
import ru.otus.springframeworkcourse.dao.QuestionsRepository;
import ru.otus.springframeworkcourse.domain.Question;
import ru.otus.springframeworkcourse.service.QuestionService;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuestionsServiceImpl implements QuestionService {

    private final List<Question> questions;
    private final List<Integer> asked;
    private final int numberOfQuestions;

    public QuestionsServiceImpl(QuestionsRepository questionsRepository,
                                ApplicationSettings applicationSettings) {
        this.questions = questionsRepository.findAll();
        this.asked = new ArrayList<>();

        int numberOfQuestions = applicationSettings.getQuestionsAsking();
        if (numberOfQuestions <= questions.size()) {
            this.numberOfQuestions = numberOfQuestions;
        } else {
            this.numberOfQuestions = questions.size();
        }
    }

    @Override
    public Question getQuestion() {
        if (questions.size() > 0) {
            while (true) {
                int random = (int) (Math.random() * numberOfQuestions);

                // if the question has already been asked
                if (asked.contains(random)) {
                    continue;
                }

                // add the question in list of asked questions
                asked.add(random);
                return questions.get(random);
            }
        } else {
            throw new IllegalStateException("There is not questions");
        }
    }
}
