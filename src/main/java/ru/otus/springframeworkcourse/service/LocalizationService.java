package ru.otus.springframeworkcourse.service;

public interface LocalizationService {

    String getLocalizedMessage(String messageKey, Object... args);
}
