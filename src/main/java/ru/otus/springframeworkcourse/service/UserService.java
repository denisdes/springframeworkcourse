package ru.otus.springframeworkcourse.service;

import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
public class UserService {

    private final Scanner scanner;
    private final LocalizationService ls;

    public UserService(LocalizationService ls) {
        this.scanner = new Scanner(System.in);
        this.ls = ls;
    }

    public String getUserName() {
        System.out.print(ls.getLocalizedMessage("user-service.first-name") + ": ");
        String firstName = scanner.nextLine();

        System.out.print(ls.getLocalizedMessage("user-service.last-name") + ": ");
        String lastName = scanner.nextLine();

        return firstName + " " + lastName;
    }

    public int getAnswer(int count) {
        int input;
        while (true) {
            System.out.print(ls.getLocalizedMessage("user-service.enter-answer") + ": ");
            if (scanner.hasNextInt()) {
                input = scanner.nextInt();
                scanner.nextLine();
                if (input <= 0 || input > count) {
                    System.out.println(ls.getLocalizedMessage("user-service.range-error") + " " + count);
                } else {
                    break;
                }
            } else {
                System.out.println(ls.getLocalizedMessage("user-service.illegal-answer"));
                scanner.nextLine();
            }
        }
        return input;
    }
}
