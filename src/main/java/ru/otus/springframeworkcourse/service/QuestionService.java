package ru.otus.springframeworkcourse.service;

import ru.otus.springframeworkcourse.domain.Question;

public interface QuestionService {

    Question getQuestion();
}
