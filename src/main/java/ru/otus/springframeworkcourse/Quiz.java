package ru.otus.springframeworkcourse;

import org.springframework.stereotype.Component;
import ru.otus.springframeworkcourse.config.ApplicationSettings;
import ru.otus.springframeworkcourse.domain.Question;
import ru.otus.springframeworkcourse.service.LocalizationService;
import ru.otus.springframeworkcourse.service.QuestionService;
import ru.otus.springframeworkcourse.service.UserService;

@Component
public class Quiz {

    private final int numberOfQuestions;
    private final QuestionService questionService;
    private final UserService userService;
    private final LocalizationService ls;

    public Quiz(QuestionService questionService,
                UserService userService,
                LocalizationService ls,
                ApplicationSettings applicationSettings) {
        this.questionService = questionService;
        this.userService = userService;
        this.numberOfQuestions = applicationSettings.getQuestionsAsking();
        this.ls = ls;
    }

    public void start() {
        // take user first name and second name and join
        String user = userService.getUserName();

        // stores the number of correct answers
        int score = 0;

        for (int i = 0; i < numberOfQuestions; i++) {
            // take random question from service
            Question question = questionService.getQuestion();

            // print question
            System.out.println("\n" + question.getQuestion() + "\n");

            // count of variants of answers
            int count = question.getVariantsOfAnswers().size();

            // print variants of answers
            for (int j = 0; j < count; j++) {
                System.out.println((j + 1) + ") " + question.getVariantsOfAnswers().get(j));
            }
            System.out.println();

            // take answer from user
            int answer = userService.getAnswer(count);

            // if answer is correct then increment score
            if (question.isCorrectAnswer(answer)) {
                score++;
            }
        }

        // print result
        System.out.println("\n" + ls.getLocalizedMessage("quiz.result", user, score, numberOfQuestions));
    }
}
