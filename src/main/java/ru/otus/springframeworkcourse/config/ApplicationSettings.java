package ru.otus.springframeworkcourse.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("application")
public class ApplicationSettings {

    private String csvFileName;
    private int questionsAsking;
    private String locale;

    public String getCsvFileName() {
        return csvFileName;
    }

    public void setCsvFileName(String csvFileName) {
        this.csvFileName = csvFileName;
    }

    public int getQuestionsAsking() {
        return questionsAsking;
    }

    public void setQuestionsAsking(int questionsAsking) {
        this.questionsAsking = questionsAsking;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
