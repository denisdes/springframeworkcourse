package ru.otus.springframeworkcourse.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.springframeworkcourse.Quiz;

@ShellComponent
public class QuizCommands {

    private final Quiz quiz;

    public QuizCommands(Quiz quiz) {
        this.quiz = quiz;
    }

    @ShellMethod("Start quiz.")
    public void start() {
        quiz.start();
    }
}
