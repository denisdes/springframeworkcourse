package ru.otus.springframeworkcourse.dao.impl;

import org.springframework.stereotype.Repository;
import ru.otus.springframeworkcourse.config.ApplicationSettings;
import ru.otus.springframeworkcourse.dao.QuestionsRepository;
import ru.otus.springframeworkcourse.domain.Question;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class FileCSVQuestionsRepositoryImpl implements QuestionsRepository {

    private final String csvFileName;

    public FileCSVQuestionsRepositoryImpl(ApplicationSettings applicationSettings) {
        this.csvFileName = applicationSettings.getCsvFileName();
    }

    @Override
    public List<Question> findAll() {
        return getQuestionsFromCsvFile(csvFileName);
    }

    private List<Question> getQuestionsFromCsvFile(String csvFileName) {
        if (csvFileName.isEmpty()) {
            throw new IllegalArgumentException("application.csv-file-name property field can not be empty");
        }

        File file = new File(csvFileName);
        if (!file.exists()) {
            throw new IllegalStateException("File " + csvFileName + " not found");
        }

        String line;
        String cvsSplitBy = ";";
        List<Question> questions = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            while ((line = br.readLine()) != null) {

                String[] items = line.split(cvsSplitBy);

                String question = items[0];
                List<String> variantsOfAnswers = Arrays.asList(items[1], items[2], items[3]);
                int correctAnswer = Integer.parseInt(items[4]);
                questions.add(new Question(question, variantsOfAnswers, correctAnswer));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return questions;
    }
}
