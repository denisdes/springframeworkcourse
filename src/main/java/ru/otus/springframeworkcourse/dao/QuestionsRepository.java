package ru.otus.springframeworkcourse.dao;

import ru.otus.springframeworkcourse.domain.Question;

import java.util.List;

public interface QuestionsRepository {

    List<Question> findAll();
}
