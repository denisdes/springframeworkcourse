package ru.otus.springframeworkcourse.service.impl;

import org.junit.Before;
import org.junit.Test;
import ru.otus.springframeworkcourse.config.ApplicationSettings;
import ru.otus.springframeworkcourse.dao.QuestionsRepository;
import ru.otus.springframeworkcourse.domain.Question;
import ru.otus.springframeworkcourse.service.QuestionService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class QuestionsServiceImplTest {

    private QuestionService questionService;

    @Before
    public void setUp() {
        QuestionsRepository mockQuestionsRepository = () -> {
            List<Question> questions = new ArrayList<>();
            List<String> variantsOfAnswers = Arrays.asList("variant1", "variant2", "variant3");
            questions.add(new Question("some question 1", variantsOfAnswers, 1));
            questions.add(new Question("some question 2", variantsOfAnswers, 2));
            return questions;
        };
        ApplicationSettings settings = new ApplicationSettings();
        settings.setQuestionsAsking(5);
        questionService = new QuestionsServiceImpl(mockQuestionsRepository, settings);
    }

    @Test
    public void thatReturnedQuestionIsNotNull() {
        Question question = questionService.getQuestion();

        assertNotNull(question);
    }

    @Test
    public void thatReturnedQuestionIsNotEmpty() {
        Question question = questionService.getQuestion();

        assertFalse(question.getQuestion().isEmpty());
        assertFalse(question.getVariantsOfAnswers().isEmpty());
    }

    @Test
    public void thatReturnedDifferentQuestions() {
        Question question1 = questionService.getQuestion();
        Question question2 = questionService.getQuestion();

        assertNotEquals(question1.getQuestion(), question2.getQuestion());
    }
}