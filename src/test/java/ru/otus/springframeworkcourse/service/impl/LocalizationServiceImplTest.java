package ru.otus.springframeworkcourse.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import ru.otus.springframeworkcourse.config.ApplicationSettings;
import ru.otus.springframeworkcourse.service.LocalizationService;

import static org.junit.Assert.assertEquals;

public class LocalizationServiceImplTest {

    private ReloadableResourceBundleMessageSource messageSource;
    private ApplicationSettings settings;
    private LocalizationService ls;

    @Before
    public void setUp() {
        messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("messages");
        messageSource.setDefaultEncoding("UTF-8");

        settings = new ApplicationSettings();
    }

    @Test
    public void testEnglishLocalizedMessages() {
        settings.setLocale("en_US");
        ls = new LocalizationServiceImpl(messageSource, settings);

        String messageKey = "user-service.first-name";
        assertEquals("Your first name", ls.getLocalizedMessage(messageKey));

        messageKey = "quiz.result";
        String user = "Test";
        int score = 5;
        int numberOfQuestions = 8;
        assertEquals("Test answered 5 of 8 questions",
                ls.getLocalizedMessage(messageKey, user, score, numberOfQuestions));
    }

    @Test
    public void testRussianLocalizedMessages() {
        settings.setLocale("ru_RU");
        ls = new LocalizationServiceImpl(messageSource, settings);

        String messageKey = "user-service.first-name";
        assertEquals("Ваше имя", ls.getLocalizedMessage(messageKey));

        messageKey = "quiz.result";
        String user = "Тест";
        int score = 5;
        int numberOfQuestions = 8;
        assertEquals("Тест ответил на 5 из 8 вопросов",
                ls.getLocalizedMessage(messageKey, user, score, numberOfQuestions));
    }
}