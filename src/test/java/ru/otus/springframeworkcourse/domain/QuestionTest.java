package ru.otus.springframeworkcourse.domain;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class QuestionTest {

    @Test
    public void isCorrectAnswer() {
        List<String> variantsOfAnswers = Arrays.asList("variant1", "variant2", "variant3");
        Question question = new Question("some question", variantsOfAnswers, 1);

        assertTrue(question.isCorrectAnswer(1));
        assertFalse(question.isCorrectAnswer(2));
    }
}